﻿using System;

namespace TRPO_Lab3.Lib
{
    public class Formula
    {
        public static double AreaSectorCircle(double R, double l) 
        {
            double S = (R * l) / 2; // R - площадь радиуса, l - длина дуги
            return S;
        }

    }
}
