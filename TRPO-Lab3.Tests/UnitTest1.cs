using NUnit.Framework;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [Test]
        public void TestAreaSectorCircle()
        {
            double R = 6;
            double l = 4;
            var expected = 12;
            var S = TRPO_Lab3.Lib.Formula.AreaSectorCircle(R, l);
            Assert.AreEqual(expected, S);

        }

        [Test]
        public void TestNull()
        {
            double R = 6;
            double l = 4;
            double S = TRPO_Lab3.Lib.Formula.AreaSectorCircle(R, l);
            if (S <= 0)
            {
                Assert.Fail("������: ������� �� ����� ���� ������ ��� ����� ����");
            }
            else
            {
                Assert.Pass("���� ������� �������");
            }
        }
    }
}